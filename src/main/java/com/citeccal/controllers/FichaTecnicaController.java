package com.citeccal.controllers;

import java.io.IOException;
import java.util.HashMap;
/*import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;*/
import java.util.List;
import java.util.Map;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
/*import org.apache.tomcat.jni.File;*/
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
/*import org.springframework.web.bind.annotation.RequestParam;*/
import org.springframework.web.bind.annotation.RestController;
/*import org.springframework.web.multipart.MultipartFile;*/
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.citeccal.imgfiles.FileStorageService;
import com.citeccal.models.FichaTecnica;
import com.citeccal.services.api.FichaTecnicaServiceAPI;
import com.citeccal.services.impl.FichaTecnicaServiceImpl;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/ficha/")
public class FichaTecnicaController {
	
	@Autowired
	private FichaTecnicaServiceAPI fichaTecnicaServiceAPI;
	
	@Autowired
	private FichaTecnicaServiceImpl fichaservice;
	
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@GetMapping(value = "/all")
	public List<FichaTecnica> getAll() {
		return fichaTecnicaServiceAPI.getAll();
	}
	
	@GetMapping(value = "/find/{id}")
	public FichaTecnica find(@PathVariable Long id) {
		return fichaTecnicaServiceAPI.get(id);
	}
	
	
	@PostMapping(value = "/save")
	public ResponseEntity<FichaTecnica> save(@RequestBody FichaTecnica fichaTecnica)
	{
		FichaTecnica obj = fichaTecnicaServiceAPI.save(fichaTecnica);
		return new ResponseEntity<FichaTecnica>(obj, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "delete/{id}")
	public ResponseEntity<Map<String, Boolean>> delete(@PathVariable Long id) {
		fichaTecnicaServiceAPI.delete(id);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value= "/{fileName}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName,HttpServletRequest request){
		
		Resource resource = fileStorageService.loadFileAsResource(fileName);
		
		String contentType = null;
		
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		}catch(IOException ex) {
			System.out.println("Could not determine fileType");
		}
		
		if(contentType==null) {
			contentType = "application/octet-stream";
		}
		
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType(contentType))
				.body(resource);
	}
	
	@Bean
    public MultipartConfigElement multipartConfigElement() {
        return new MultipartConfigElement("");
    }
		
	@PostMapping(value = "/upload")
    public ResponseEntity<FichaTecnica> uploadFile(@RequestPart(name = "file") List<MultipartFile> file, @RequestParam Long id) {
		String fileName = null;
		String fileName2 = null;
		String fileName3 = null;
		String fileName4 = null;
  
        	fileName = fileStorageService.storeFile(file.get(0));
        	fileName2 = fileStorageService.storeFile(file.get(1));
    		fileName3 = fileStorageService.storeFile(file.get(2));
    		fileName4 = fileStorageService.storeFile(file.get(3));
 
    	String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
    				.path("/api/ficha/")
    				.path(fileName)
    				.toUriString();
    	String fileDownloadUri2 = ServletUriComponentsBuilder.fromCurrentContextPath()
    				.path("/api/ficha/")
    				.path(fileName2)
    				.toUriString();
    	String fileDownloadUri3 = ServletUriComponentsBuilder.fromCurrentContextPath()
    				.path("/api/ficha/")
    				.path(fileName3)
    				.toUriString();
    	String fileDownloadUri4 = ServletUriComponentsBuilder.fromCurrentContextPath()
    				.path("/api/ficha/")
    				.path(fileName4)
    				.toUriString();
    	FichaTecnica obj = fichaTecnicaServiceAPI.get(id);
    
    	obj.setImgRuta1("http:192.168.0.84:8081/api/ficha/"
    			+ fileName);
    	obj.setImg1(fileName);
    	obj.setImgRuta2("http:192.168.0.84:8081/api/ficha/"
    			+ fileName2);
    	obj.setImg2(fileName2);
    	obj.setImgRuta3("http:192.168.0.84:8081/api/ficha/"
    			+ fileName3);
    	obj.setImg3(fileName3);
    	obj.setImgRuta4("http:192.168.0.84:8081/api/ficha/"
    			+ fileName4);
    	obj.setImg4(fileName4);
    	FichaTecnica f = fichaTecnicaServiceAPI.save(obj);
    	return new ResponseEntity<FichaTecnica>(f, HttpStatus.OK);
    } 
	
	
	@PutMapping("/{id}")
	public ResponseEntity<FichaTecnica> updateFicha(@PathVariable Long id, @RequestBody FichaTecnica f){
		FichaTecnica ficha = fichaTecnicaServiceAPI.get(id);
		ficha.setFichaId(f.getFichaId());
		ficha.setLinea(f.getLinea());
		ficha.setSerie(f.getSerie());
		ficha.setAlTaco(f.getAlTaco());
		ficha.setColor(f.getColor());
		ficha.setEstilo(f.getEstilo());
		ficha.setCodHorma(f.getCodHorma());
		ficha.setCodPlanta(f.getCodPlanta());
		ficha.setC1Tipo(f.getC1Tipo());
		ficha.setC1Color(f.getC1Color());
		ficha.setC2Tipo(f.getC2Tipo());
		ficha.setC2Color(f.getC2Color());
		ficha.setC3Tipo(f.getC3Tipo());
		ficha.setC3Color(f.getC3Color());
		ficha.setF1Tipo(f.getF1Tipo());
		ficha.setF1Color(f.getF1Color());
		ficha.setF2Tipo(f.getF2Tipo());
		ficha.setF2Color(f.getF2Color());
		ficha.setH1Tipo(f.getH1Tipo());
		ficha.setH1Numero(f.getH1Numero());
		ficha.setH1Color(f.getH1Color());
		ficha.setH2Tipo(f.getH2Tipo());
		ficha.setH2Numero(f.getH2Numero());
		ficha.setH2Color(f.getH2Color());
		ficha.setH3Tipo(f.getH3Tipo());
		ficha.setH3Numero(f.getH3Numero());
		ficha.setH3Color(f.getH3Color());
		ficha.setAcc1Detalle(f.getAcc1Detalle());
		ficha.setAcc1Material(f.getAcc1Material());
		ficha.setAcc1Color(f.getAcc1Color());
		ficha.setAcc2Detalle(f.getAcc2Detalle());
		ficha.setAcc2Material(f.getAcc2Material());
		ficha.setAcc2Color(f.getAcc2Color());
		ficha.setAcc3Detalle(f.getAcc3Detalle());
		ficha.setAcc3Material(f.getAcc3Material());
		ficha.setAcc3Color(f.getAcc3Color());
		ficha.setCierreDetalle(f.getCierreDetalle());
		ficha.setCierreMaterial(f.getCierreMaterial());
		ficha.setCierreColor(f.getCierreColor());
		ficha.setPullerDetalle(f.getPullerDetalle());
		ficha.setPullerMaterial(f.getPullerMaterial());
		ficha.setPullerColor(f.getPullerColor());
		ficha.setPunteraMaterial(f.getPunteraMaterial());
		ficha.setContraMaterial(f.getContraMaterial());
		ficha.setP1Material(f.getP1Material());
		ficha.setP1Color(f.getP1Color());
		ficha.setP1Forrado(f.getP1Forrado());
		ficha.setP2Material(f.getP2Material());
		ficha.setP2Color(f.getP2Color());
		ficha.setP2Forrado(f.getP2Forrado());
		ficha.setPlataformaMaterial(f.getPlataformaMaterial());
		ficha.setPlataformaColor(f.getPlataformaColor());
		ficha.setPlataformaForrado(f.getPlataformaForrado());
		ficha.setTacoMaterial(f.getTacoMaterial());
		ficha.setTacoColor(f.getTacoColor());
		ficha.setTacoForrado(f.getTacoForrado());
		ficha.setPlantillaMaterial(f.getPlantillaMaterial());
		ficha.setPlantillaColor(f.getPlantillaColor());
		ficha.setAcolcheMaterial(f.getAcolcheMaterial());
		ficha.setH4Tipo(f.getH4Tipo());
		ficha.setH4Numero(f.getH4Numero());
		ficha.setH4Color(f.getH4Color());
		ficha.setCosturaTipo(f.getCosturaTipo());
		ficha.setSelloMarca(f.getSelloMarca());
		ficha.setSelloTipo(f.getSelloTipo());
		ficha.setSelloMaterial(f.getSelloMaterial());
		ficha.setCueritoMarca(f.getCueritoMarca());
		ficha.setCueritoTipo(f.getCueritoTipo());
		ficha.setCueritoMaterial(f.getCueritoMaterial());
		ficha.setHantagMarca(f.getHantagMarca());
		
		FichaTecnica updateficha = fichaTecnicaServiceAPI.save(ficha);
		return ResponseEntity.ok(updateficha);
		
	}

}
